import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpXhrBackend } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MediaItemComponent } from './media/media-item.component';
import { MediaItemListComponent } from './media-list/media-item-list.component';
import { CategoryListPipe } from './media-list/category-list.pipe';
import { MediaItemFormComponent } from './form-component/media-item-form.component';
import { MediaItemModelFormComponent } from './form-model-driven/media-item-form.component';
// bootstrap start
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {NgbdModalBasic} from './bootstrap/modal/modal-basic';
import {NgbdRatingForm} from './bootstrap/rating-form/rating-form';
import { NgbdSortableHeader } from './bootstrap/table/sortable.directive';
import { NgbdTableComplete } from './bootstrap/table/table-complete';

import { CountryHttpService } from './bootstrap/table/country-http-service';
// bootstrap end
import { MediaItemService } from './service/media-item-service';

import { FavoriteDirective } from './media/favorite.directve';
import {providerDef} from '@angular/compiler/src/view_compiler/provider_compiler';
import {lookupListToken, lookupLists} from './providers';
// import {MockXHRBackend} from './mock-xhr-backend';
import {routing} from './app.routing';


import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';

registerLocaleData(localeFr, 'fr-FR', localeFrExtra);

// const lookupLists = {mediums : ['Movies', 'Series', 'Films']};

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    routing
  ],
  declarations: [
    AppComponent,
    MediaItemComponent,
    MediaItemListComponent,
    FavoriteDirective,
    CategoryListPipe,
    MediaItemFormComponent,
    MediaItemModelFormComponent,
    NgbdModalBasic,
    NgbdRatingForm,
    NgbdSortableHeader,
    NgbdTableComplete
  ],
  // exports: [NgbdModalBasic],
  providers: [
    CountryHttpService,
    MediaItemService,
    {provide : lookupListToken, useValue: lookupLists},
    // {provide : MockXHRBackend, useValue: MockXHRBackend}
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule {}
