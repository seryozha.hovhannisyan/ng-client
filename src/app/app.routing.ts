import {Routes, RouterModule} from '@angular/router';
import {MediaItemFormComponent} from './form-component/media-item-form.component';
import {MediaItemModelFormComponent} from './form-model-driven/media-item-form.component';
import {MediaItemListComponent} from './media-list/media-item-list.component';
import {NgbdModalBasic} from './bootstrap/modal/modal-basic';
import {NgbdTableComplete} from './bootstrap/table/table-complete';

const appRoutes: Routes = [
  {path: 'add', component: MediaItemModelFormComponent},
  {path: 'bootstrap/modal', component: NgbdModalBasic},
  {path: 'bootstrap/table', component: NgbdTableComplete},
  {path: ':medium', component: MediaItemListComponent},
  // todo ask to Hro
  // {path: '', pathMatch: 'full', redirectTo: 'kuku' }
];

export const routing = RouterModule.forRoot(appRoutes);
