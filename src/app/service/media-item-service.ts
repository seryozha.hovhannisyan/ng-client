import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';

@Injectable()
export class MediaItemService {

  mediaItems = [
    {
      id: 1,
      name: 'Firebug List',
      medium: 'Series',
      category: 'Science Fiction',
      year: 2010,
      watchedOn: 1294166565384,
      isFavorite: false
    },
    {
      id: 2,
      name: 'The Small Tall',
      medium: 'Movies',
      category: 'Comedy',
      year: 2015,
      watchedOn: null,
      isFavorite: true
    }, {
      id: 3,
      name: 'The Redemption',
      medium: 'Movies',
      category: 'Action',
      year: 2016,
      watchedOn: null,
      isFavorite: false
    }, {
      id: 4,
      name: 'Hoopers',
      medium: 'Series',
      category: 'Drama',
      year: null,
      watchedOn: null,
      isFavorite: true
    }, {
      id: 5,
      name: 'Happy Joe: Cheery Road',
      medium: 'Movies',
      category: 'Action',
      year: 2015,
      watchedOn: 1457166565384,
      isFavorite: false
    }
  ];
  constructor(private httpClient: HttpClient) {}
  // todo ask to Hro
  // get() {
  //   // return this.mediaItems;
  //   return this.httpClient.get<MediaItemsResponse>('http://localhost:9000/api/mediaItems').pipe(
  //     map(
  //       (response: MediaItemsResponse) => {
  //         return response.mediaItems;
  //       }
  //     )
  //   );
  // }
  get(name: string) {
    // return this.mediaItems;
    const options = {
      params: {
        name
      }
    };
    return this.httpClient.get<MediaItemsResponse>('http://localhost:9000/api/mediaItems', options).pipe(
      map(
        (response: MediaItemsResponse) => {
          return response.mediaItems;
        }
      )
    );
  }
  add(mediaItem) {
    // this.mediaItems.push(mediaItem);
    // return this.httpClient.post('http://localhost:9000/api/mediaItems', mediaItem);
    // todo remove after beckend implementatoin
    return this.httpClient.get<MediaItemsResponse>('http://localhost:9000/api/mediaItems').pipe(
      map(
        (response: MediaItemsResponse) => {
          return response.mediaItems;
        }
      )
    );
  }
  delete(mediaItem) {
    // const i = this.mediaItems.indexOf(mediaItem);
    // if (i >= 0) {
    //   this.mediaItems.splice(i, 1);
    // }
    return this.httpClient.delete('http://localhost:9000/api/mediaItems/${mediaItem.id}');
  }
}
interface MediaItem {
  aid: number;
  name: string;
  medium: string;
  category: string;
  year: number;
  watchedOn: string;
  isFavorite: boolean;
}
interface MediaItemsResponse {
  // todo ask to Hro
  mediaItems: MediaItem[];
}
