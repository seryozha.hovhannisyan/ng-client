import {Component, OnInit} from '@angular/core';
import {MediaItemService} from '../service/media-item-service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-mw-media-item-list',
  templateUrl: 'media-item-list.component.html',
  styleUrls: ['media-item-list.component.css']
})
export class MediaItemListComponent implements OnInit {

  public name = 'ello';
  mediaItems = [];

  constructor(private mediaItemService: MediaItemService,
              private activatedRoute: ActivatedRoute) {
  }
  ngOnInit() {
    // this.mediaItems = this.mediaItemService.get();
    // this.mediaItemService.get().subscribe(
    //   mediaItems => {
    //     return this.mediaItems = mediaItems;
    //   }
    // );
    this.activatedRoute.params.subscribe(params => {
      // let medium = params['medium'];
      let medium = params.medium;
      if (medium.toLowerCase() === 'kuku') {
        medium = this.name;
      }
      this.getMediaItems(medium);
    });
  }

  // getMediaItems() {
  //   // todo never calls from html
  //   console.log(this.name);
  //   this.getMediaItems(this.name);
  // }
  getMediaItems(name: string) {
    console.log('2', this.name);
    console.log('3', name);
    this.mediaItemService.get(name).subscribe(
      mediaItems => {
        return this.mediaItems = mediaItems;
      }
    );
  }

  onMediaItemDelete(mediaItem) {
    this.mediaItemService.delete(mediaItem).subscribe(() => {

    });
  }

}
