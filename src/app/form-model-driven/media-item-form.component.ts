import {Component, Inject, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder  } from '@angular/forms';
import {MediaItemService} from '../service/media-item-service';
import {lookupListToken} from '../providers';
import {Router} from '@angular/router';

@Component({
  selector: 'app-mw-media-item-model-form',
  templateUrl: 'media-item-form.component.html',
  styleUrls: ['media-item-form.component.css']
})
export class MediaItemModelFormComponent implements OnInit {
  form;
  constructor(private formBuilder: FormBuilder,
              private mediaItemService: MediaItemService,
              @Inject(lookupListToken) public lookupLists,
              private router: Router) {}
  ngOnInit() {
    // this.form = new FormGroup({
    //   medium : new FormControl('Series'),
    //   // name : new FormControl('', Validators.pattern('[\\\\w\\\\-\\\\s\\\\/]+') ),
    //   name : new FormControl('', Validators.email ),
    //   // for numbers
    //   // name : new FormControl('', Validators.min(3) ),
    //   // name : new FormControl('' , Validators.compose[Validators.required] ),
    //   category : new FormControl(''),
    //   year : new FormControl('2019', this.yearValidator),
    // });

    this.form = this.formBuilder.group({
      medium : this.formBuilder.control('Series'),
      // name : new FormControl('', Validators.pattern('[\\\\w\\\\-\\\\s\\\\/]+') ),
      name : this.formBuilder.control('', Validators.email ),
      // for numbers
      // name : new FormControl('', Validators.min(3) ),
      // name : new FormControl('' , Validators.compose[Validators.required] ),
      category : this.formBuilder.control(''),
      year : this.formBuilder.control('2019', this.yearValidator),
    });

  }
  yearValidator(control) {
    if (control.value.trim().length === 0) {
      return null;
    }
    const year = parseInt(control.value);
    if (year > 1900 && year < 2100) {
      return null;
    } else {
      // return {year : true};
      return {year : {min : 1900, max : 2100}};
    }

  }
  onSubmit(mediaItem) {
    this.mediaItemService.add(mediaItem).subscribe(() => {
      console.log('Model-driven added', mediaItem);
      this.router.navigate(['/', mediaItem.medium]);
    });
  }
}
